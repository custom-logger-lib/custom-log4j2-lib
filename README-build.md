
![release](https://gitlab.com/custom-logger-lib/custom-log4j2-lib/-/badges/release.svg)
![coverage](https://gitlab.com/custom-logger-lib/custom-log4j2-lib/badges/main/coverage.svg)

# Build config

This project use [ToBeContinuous](https://to-be-continuous.gitlab.io/doc/) Gitlab-CI templates 


## SonarQube configuration

### prerequisites

- create a free account on sonarcloud.io
- copy (and backup) your SonarCloud.io's account token 
- into Gitlab _Settings > CI/CD > Variables_, create a `SONAR_TOKEN` variable and paste the value of your SonarCloud.io account token


### In project's `gitlab-ci.yml` or in project's ci/cd variables

- add a `SONAR_HOST_URL` variable:
  ```yaml
  variables:
    SONAR_HOST_URL: "https://sonarcloud.io"` # activate ToBeContinuous's sonar tasks
  ```


# Maven release and deployment

- In `gitlab-ci.yml` add:

  ```yaml
  variables:
    MAVEN_DEPLOY_ENABLED: "true" # activate ToBeContinuous's release and deploy tasks
  ```

- In a terminal: create a new password-less ed25519 key:

  ```bash
  ssh-keygen -t ed25519 -C "gitlab-access-key"
  ```
- In GitLab:
  
  - **PUBLIC KEY**: In project's _Settings > Repository > Deploy keys_: create a new Deploy Key filled with the content of the **public** ed35519 key (should start with: "`ssh-ed25519 AAAA`") making sure to enable _write access to the project_
  - **PRIVATE KEY**: In project's _Settings > CI/CD > Variables_: create a `GIT_PRIVATE_KEY` variable filled with the content of the **private** ed35519 key (should start with: "`-----BEGIN OPENSSH PRIVATE KEY-----`")


- In `pom.xml`: 
  - Setup the `scm` element exactly as bellow:
    ```xml
    <scm>
      <connection>scm:git:git@gitlab.com:custom-logger-lib/custom-log4j2-lib.git</connection>
      <developerConnection>scm:git:git@gitlab.com:custom-logger-lib/custom-log4j2-lib.git</developerConnection>
    </scm>
    ```
  - Setup the `distributionManagement` element exactly as bellow:
    ```xml
    <distributionManagement>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <!--suppress UnresolvedMavenProperty -->
            <url>${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
        </snapshotRepository>
        <repository>
            <id>gitlab-maven</id>
            <!--suppress UnresolvedMavenProperty -->
            <url>${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
        </repository>
    </distributionManagement>
    ```
- Create or update `.m2/settings.xml`:
  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  
      <servers>
          <!-- allow deploying the jar artifact on GitLab's package registry -->
          <!-- see: https://docs.gitlab.com/ee/user/packages/maven_repository/index.html#use-the-gitlab-endpoint-for-maven-packages -->
          <server>
              <id>gitlab-maven</id>
              <configuration>
                  <httpHeaders>
                      <property>
                          <name>Job-Token</name>
                          <value>${env.CI_JOB_TOKEN}</value>
                      </property>
                  </httpHeaders>
              </configuration>
          </server>
      </servers>
  
  </settings>
  ```


EOF
