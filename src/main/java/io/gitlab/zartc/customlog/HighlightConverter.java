/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache license, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the license for the specific language governing permissions and
 * limitations under the license.
 */
package io.gitlab.zartc.customlog;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ServiceLoader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.apache.logging.log4j.core.pattern.AnsiEscape;
import org.apache.logging.log4j.core.pattern.ConverterKeys;
import org.apache.logging.log4j.core.pattern.LogEventPatternConverter;
import org.apache.logging.log4j.core.pattern.PatternConverter;
import org.apache.logging.log4j.core.pattern.PatternFormatter;
import org.apache.logging.log4j.util.Strings;

import io.gitlab.zartc.customlog.customstyles.CustomStyles;
import io.gitlab.zartc.customlog.customstyles.CustomStylesProvider;

/**
 * Highlight pattern converter. Formats the result of a pattern using a color appropriate for the Level in the LogEvent.
 * <p>
 * For example:
 * </p>
 *
 * <pre>
 * %highlight{%d{ ISO8601 } [%t] %-5level: %msg%n%throwable}
 * </pre>
 * <p>
 * You can define custom colors for each Level:
 * </p>
 *
 * <pre>
 * %highlight{%d{ ISO8601 } [%t] %-5level: %msg%n%throwable}{FATAL=red, ERROR=red, WARN=yellow, INFO=green, DEBUG=cyan,
 * TRACE=black}
 * </pre>
 * <p>
 * You can use a predefined style:
 * </p>
 *
 * <pre>
 * %highlight{%d{ ISO8601 } [%t] %-5level: %msg%n%throwable}{STYLE=DEFAULT}
 * </pre>
 * <p>
 * The available predefined styles are:
 * </p>
 * <ul>
 * <li>{@code Default}</li>
 * <li>{@code Log4j} - The same as {@code Default}</li>
 * <li>{@code Logback}</li>
 * </ul>
 * <p>
 * You can use whitespace around the comma and equal sign. The names in values MUST come from the
 * {@linkplain AnsiEscape} enum, case is normalized to upper-case internally.
 * </p>
 *
 * <p>
 * To disable ANSI output unconditionally, specify an additional option <code>disableAnsi=true</code>, or to
 * disable ANSI output if no console is detected, specify option <code>noConsoleNoAnsi=true</code> e.g..
 * </p>
 * <pre>
 * %highlight{%d{ ISO8601 } [%t] %-5level: %msg%n%throwable}{STYLE=DEFAULT, noConsoleNoAnsi=true}
 * </pre>
 */
@Plugin(name = "custom-highlight", category = PatternConverter.CATEGORY)
@ConverterKeys({ "hilight" })
public final class HighlightConverter extends LogEventPatternConverter {

    private static final String STYLE_SCHEME_KEY = "STYLE";

    private static final CustomStyles STYLES;

    static {
        ServiceLoader<CustomStylesProvider> customStylesProviders = ServiceLoader.load(CustomStylesProvider.class);
        CustomStylesProvider customStylesProvider = customStylesProviders.iterator().next();
        STYLES = customStylesProvider.create();
    }

    /**
     * Creates a level style map where values are ANSI escape sequences given configuration options in {@code option[1]}
     * .
     * <p>
     * The format of the option string in {@code option[1]} is:
     * </p>
     *
     * <pre>
     * Level1=Value, Level2=Value, ...
     * </pre>
     *
     * <p>
     * For example:
     * </p>
     *
     * <pre>
     * ERROR=red bold, WARN=yellow bold, INFO=green, ...
     * </pre>
     *
     * <p>
     * You can use whitespace around the comma and equal sign. The names in values MUST come from the
     * {@linkplain AnsiEscape} enum, case is normalized to upper-case internally.
     * </p>
     *
     * @param options The second slot can optionally contain the style map.
     *
     * @return a new map
     */
    private static Map<String, String> createLevelStyleMap(final String[] options) {
        if (options.length < 2) {
            return STYLES.getDefault();
        }

        final var levelStyles = new HashMap<>(STYLES.getDefault());
        // Feels like a hack. Should String[] options change to a Map<String,String>?
        final var stylesString = sanitizeStyleString(options[1]);
        final var styles = AnsiEscape.createMap(stylesString, new String[] { STYLE_SCHEME_KEY });

        for (final var entry : styles.entrySet()) {
            final var key = entry.getKey().toUpperCase(Locale.ENGLISH);
            final var value = entry.getValue();
            if (STYLE_SCHEME_KEY.equalsIgnoreCase(key)) {
                registerStyleScheme(value, levelStyles);
            } else {
                registerLevelStyle(key, value, levelStyles);
            }
        }

        return levelStyles;
    }

    private static String sanitizeStyleString(String stylesString) {
        return stylesString
                .replaceAll("disableAnsi=(true|false)", Strings.EMPTY)
                .replaceAll("noConsoleNoAnsi=(true|false)", Strings.EMPTY);
    }

    private static void registerLevelStyle(String levelName, String style, HashMap<String, String> levelStyles) {
        final var level = Level.toLevel(levelName, null);
        if (level == null) {
            LOGGER.warn("Setting style for yet unknown level name {}", levelName);
            levelStyles.put(levelName, style);
        } else {
            levelStyles.put(level.name(), style);
        }
    }

    private static void registerStyleScheme(String styleSuiteName, HashMap<String, String> levelStyles) {
        final var enumMap = STYLES.get(styleSuiteName.toUpperCase(Locale.ENGLISH));
        if (enumMap == null) {
            if (LOGGER.isErrorEnabled()) {
                LOGGER.error("Unknown level style: {}. Use one of {}", styleSuiteName, Arrays.toString(STYLES.list()));
            }
        } else {
            levelStyles.putAll(enumMap);
        }
    }

    /**
     * Gets an instance of the class.
     *
     * @param config The current Configuration.
     * @param options pattern options, first string is the pattern and is mandatory, second string is the format and is optional.
     *
     * @return instance of class.
     */
    public static HighlightConverter newInstance(final Configuration config, final String[] options) {
        if (options == null || options.length == 0 || options[0] == null) {
            LOGGER.error("No pattern supplied on style");
            return null;
        }

        final var parser = PatternLayout.createPatternParser(config);
        final var formatters = parser.parse(options[0]);
        final var disableAnsi = Arrays.toString(options).contains("disableAnsi=true");
        final var noConsoleNoAnsi = Arrays.toString(options).contains("noConsoleNoAnsi=true");
        final var hideAnsi = disableAnsi || (noConsoleNoAnsi && System.console() == null);

        return new HighlightConverter(formatters, createLevelStyleMap(options), hideAnsi);
    }

    private final Map<String, String> levelStyles;

    private final List<PatternFormatter> patternFormatters;

    private final boolean noAnsi;

    /**
     * Construct the converter.
     *
     * @param patternFormatters The PatternFormatters to generate the text to manipulate.
     * @param noAnsi If true, do not output ANSI escape codes.
     */
    private HighlightConverter(final List<PatternFormatter> patternFormatters, final Map<String, String> levelStyles, final boolean noAnsi) {
        super("style", "style");
        this.patternFormatters = patternFormatters;
        this.levelStyles = levelStyles;
        this.noAnsi = noAnsi;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void format(final LogEvent event, final StringBuilder toAppendTo) {
        final var levelStyle = levelStyles.get(event.getLevel().name());
        var start = 0;
        var end = 0;
        if (!noAnsi) { // use ANSI: set prefix
            start = toAppendTo.length();
            if (levelStyle != null) {
                toAppendTo.append(levelStyle);
            }
            end = toAppendTo.length();
        }

        // noinspection ForLoopReplaceableByForEach
        for (int i = 0, size = patternFormatters.size(); i < size; i++) {
            patternFormatters.get(i).format(event, toAppendTo);
        }

        // if we use ANSI we need to add the postfix or erase the unnecessary prefix
        if (!noAnsi) {
            var empty = toAppendTo.length() == end;
            if (empty) {
                toAppendTo.setLength(start); // erase prefix
            } else if (levelStyle != null) {
                toAppendTo.append(AnsiEscape.getDefaultStyle()); // add postfix
            }
        }
    }

    String getLevelStyle(final Level level) {
        return levelStyles.get(level.name());
    }

    @Override
    public boolean handlesThrowable() {
        for (PatternFormatter formatter : patternFormatters) {
            if (formatter.handlesThrowable()) {
                return true;
            }
        }

        return false;
    }
}
