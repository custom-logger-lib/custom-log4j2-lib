package io.gitlab.zartc.customlog;

import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.arbiters.Arbiter;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;
import org.apache.logging.log4j.status.StatusLogger;


/**
 * A custom <a href="https://logging.apache.org/log4j/2.x/manual/configuration.html#Arbiters">Log4j2 Arbiter</a>
 * to determine whether or not the log output should be in text plain or text json.
 *
 * @see <a href="https://www.baeldung.com/log4j2-plugins">log4j2-plugins</a>
 */
@Plugin(name = "PlatformArbiter",
        category = Node.CATEGORY,
        elementType = Arbiter.ELEMENT_TYPE,
        deferChildren = true)
public class PlatformArbiter implements Arbiter {

    private final String propertyName;
    private final String matchingValue;

    public PlatformArbiter(String propertyName, String matchingValue) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("PlatformArbiter missing 'propertyName' attribute");
        }
        if (matchingValue == null || matchingValue.trim().isEmpty()) {
            throw new IllegalArgumentException("PlatformArbiter missing 'matchingValue' attribute");
        }

        this.propertyName = propertyName.trim();
        this.matchingValue = matchingValue.trim();
    }

    /**
     * Returns true if the running platform is considered DEV, false otherwise.
     */
    @Override
    public boolean isCondition() {
        if (propertyName != null) {
            final var varFinder = createVarFinder();
            final var lookupResult = varFinder.lookup(propertyName);
            if (lookupResult.getValue() != null) {
                final var result = lookupResult.match(matchingValue);

                StatusLogger.getLogger().debug("PlatformArbiter: '{}:{}={}' match '{}' => {}",
                        lookupResult.getSource(), lookupResult.getVariable(), lookupResult.getValue(), matchingValue, result);

                return result;
            }
        }

        // else if running on Windows (like when running from your IDE) => true (i.e DEV platform)
        final var osName = getOsName();
        final var result = osName.toLowerCase().contains("windows");

        StatusLogger.getLogger().debug("PlatformArbiter: 'sys:os.name={}' match '{}' => {}",
                osName, "windows", result);

        return result;
    }

    protected String getOsName() {
        return System.getProperty("os.name", "undefined");
    }

    protected VarFinder createVarFinder() {
        return new VarFinder();
    }

    @PluginBuilderFactory
    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder implements org.apache.logging.log4j.core.util.Builder<PlatformArbiter> {

        @PluginBuilderAttribute("propertyName")
        private String propertyName;

        @PluginBuilderAttribute("matchingValue")
        private String matchingValue;

        /**
         * Sets the Property Name.
         *
         * @param propertyName the property name.
         *
         * @return this
         */
        public Builder setPropertyName(final String propertyName) {
            this.propertyName = propertyName;
            return this;
        }

        /**
         * Sets the Property Value.
         *
         * @param matchingValue the property value.
         *
         * @return this
         */
        public Builder setMatchingValue(final String matchingValue) {
            this.matchingValue = matchingValue;
            return this;
        }

        public Builder asBuilder() {
            return this;
        }

        public PlatformArbiter build() {
            return new PlatformArbiter(propertyName, matchingValue);
        }
    }
}
