package io.gitlab.zartc.customlog;

import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.arbiters.Arbiter;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginBuilderFactory;
import org.apache.logging.log4j.status.StatusLogger;


/**
 * A custom <a href="https://logging.apache.org/log4j/2.x/manual/configuration.html#Arbiters">Log4j2 Arbiter</a>
 * to determine whether or not the log output should be in text plain or text json.
 *
 * @see <a href="https://www.baeldung.com/log4j2-plugins">log4j2-plugins</a>
 */
@Plugin(name = "VarArbiter",
        category = Node.CATEGORY,
        elementType = Arbiter.ELEMENT_TYPE,
        deferChildren = true)
public class VarArbiter implements Arbiter {

    private final String propertyName;
    private final String matchingValue;

    public VarArbiter(String propertyName, String matchingValue) {
        if (propertyName == null || propertyName.trim().isEmpty()) {
            throw new IllegalArgumentException("VarArbiter missing 'propertyName' attribute");
        }
        if (matchingValue == null || matchingValue.trim().isEmpty()) {
            throw new IllegalArgumentException("VarArbiter missing 'matchingValue' attribute");
        }

        this.propertyName = propertyName.trim();
        this.matchingValue = matchingValue.trim();
    }

    /**
     * Returns true if the env var or system property is defined and its effective value matches the requested value.
     */
    @Override
    public boolean isCondition() {
        final var varFinder = createVarFinder();
        final var lookupResult = varFinder.lookup(propertyName);
        final var result = lookupResult.match(matchingValue);

        StatusLogger.getLogger().debug("VarArbiter: '{}:{}={}' match '{}' => {}",
                lookupResult.getSource(), lookupResult.getVariable(), lookupResult.getValue(), matchingValue, result);

        return result;
    }

    protected VarFinder createVarFinder() {
        return new VarFinder();
    }


    @PluginBuilderFactory
    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder implements org.apache.logging.log4j.core.util.Builder<VarArbiter> {

        @PluginBuilderAttribute("propertyName")
        private String propertyName;

        @PluginBuilderAttribute("matchingValue")
        private String matchingValue;

        /**
         * Sets the Property Name.
         *
         * @param propertyName the property name.
         *
         * @return this
         */
        public Builder setPropertyName(final String propertyName) {
            this.propertyName = propertyName;
            return this;
        }

        /**
         * Sets the value that will make this arbiter true.
         * If the property's effective value match the matching value then the arbiter will return true.
         *
         * @param matchingValue the matching value.
         *
         * @return this
         */
        public Builder setMatchingValue(final String matchingValue) {
            this.matchingValue = matchingValue;
            return this;
        }

        public Builder asBuilder() {
            return this;
        }

        public VarArbiter build() {
            return new VarArbiter(propertyName, matchingValue);
        }
    }
}
