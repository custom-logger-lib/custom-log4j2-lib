package io.gitlab.zartc.customlog;

public class VarFinder {
    public static final String SOURCE_NOT_FOUND = "not-found";
    public static final String SOURCE_ENV = "env";
    public static final String SOURCE_SYS = "sys";


    public static class Result {
        /**
         * The variable name.
         */
        private final String variable;
        /**
         * Where the variable was found: "env:", "sys:" or "not-found:".
         */
        private final String source;
        /**
         * What value was found or {@code null} when not found.
         */
        private final String value;

        public Result(String variable, String source, String value) {
            this.variable = variable;
            this.source = source;
            this.value = value;
        }

        public boolean matchExactly(String expectedValue) {
            return value != null && (expectedValue == null || value.trim().equalsIgnoreCase(expectedValue.trim()));
        }

        public boolean match(String expectedValue) {
            return value != null && (expectedValue == null || value.trim().toUpperCase().contains(expectedValue.trim().toUpperCase()));
        }

        public String getVariable() {
            return this.variable;
        }

        public String getSource() {
            return this.source;
        }

        public String getValue() {
            return this.value;
        }
    }

    public Result lookup(String variable) {
        var source = SOURCE_NOT_FOUND;
        String value = null;

        if (variable != null) {
            if ((value = getEnv(variable)) != null) {
                source = SOURCE_ENV;
            } else if ((value = getSys(variable)) != null) {
                source = SOURCE_SYS;
            }
        }

        return new Result(variable, source, value);
    }

    protected String getEnv(String variable) {
        return System.getenv(variable);
    }

    protected String getSys(String variable) {
        return System.getProperty(variable);
    }
}
