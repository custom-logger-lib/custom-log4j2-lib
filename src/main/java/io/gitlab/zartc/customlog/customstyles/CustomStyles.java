package io.gitlab.zartc.customlog.customstyles;

import java.util.Map;

public interface CustomStyles {

    Map<String, String> getDefault();

    Map<String, String> get(String name);

    String[] list();
}

/* EOF */
