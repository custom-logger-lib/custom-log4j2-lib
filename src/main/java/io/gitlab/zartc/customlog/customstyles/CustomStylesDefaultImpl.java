package io.gitlab.zartc.customlog.customstyles;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.pattern.AnsiEscape;

public class CustomStylesDefaultImpl implements CustomStyles {
    protected Map<String, Map<String, String>> styleMap = Map.of(

            "DEFAULT", defaultStyle(),
            "LOGBACK", logbackStyle(),
            "LOG4J", log4jStyle(),
            "ZION", zionStyle());

    public static Map<String, String> defaultStyle() {
        var style = new HashMap<String, String>();
        // @formatter:off
        style.put(Level.FATAL.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.ERROR.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.WARN.name(),  AnsiEscape.createSequence("RED"));
        style.put(Level.INFO.name(),  AnsiEscape.createSequence((String[]) null));
        style.put(Level.DEBUG.name(), AnsiEscape.createSequence("MAGENTA"));
        style.put(Level.TRACE.name(), AnsiEscape.createSequence("BRIGHT", "MAGENTA"));
        // @formatter:on

        return style;
    }

    public static Map<String, String> logbackStyle() {
        var style = new HashMap<String, String>();
        // @formatter:off
        style.put(Level.FATAL.name(), AnsiEscape.createSequence("BLINK", "BRIGHT", "RED"));
        style.put(Level.ERROR.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.WARN.name(),  AnsiEscape.createSequence("RED"));
        style.put(Level.INFO.name(),  AnsiEscape.createSequence("BLUE"));
        style.put(Level.DEBUG.name(), AnsiEscape.createSequence((String[]) null));
        style.put(Level.TRACE.name(), AnsiEscape.createSequence((String[]) null));
        // @formatter:on

        return style;
    }

    private static Map<String, String> log4jStyle() {
        var style = new HashMap<String, String>();
        // @formatter:off
        style.put(Level.FATAL.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.ERROR.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.WARN.name(),  AnsiEscape.createSequence("YELLOW"));
        style.put(Level.INFO.name(),  AnsiEscape.createSequence("GREEN"));
        style.put(Level.DEBUG.name(), AnsiEscape.createSequence("CYAN"));
        style.put(Level.TRACE.name(), AnsiEscape.createSequence("BLACK"));
        // @formatter:on

        return style;
    }

    private static Map<String, String> zionStyle() {
        var style = new HashMap<String, String>();
        // @formatter:off
        style.put(Level.FATAL.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.ERROR.name(), AnsiEscape.createSequence("BRIGHT", "RED"));
        style.put(Level.WARN.name(),  AnsiEscape.createSequence("YELLOW"));
        style.put(Level.INFO.name(),  AnsiEscape.createSequence("GREEN"));
        style.put(Level.DEBUG.name(), AnsiEscape.createSequence("BLACK"));
        style.put(Level.TRACE.name(), AnsiEscape.createSequence("BLACK"));
        // @formatter:on

        return style;
    }

    public Map<String, String> getDefault() {
        return styleMap.get("DEFAULT");
    }

    @Override
    public Map<String, String> get(String name) {
        return styleMap.get(name);
    }

    @Override
    public String[] list() {
        return styleMap.keySet().toArray(new String[0]);
    }
}

/* EOF */
