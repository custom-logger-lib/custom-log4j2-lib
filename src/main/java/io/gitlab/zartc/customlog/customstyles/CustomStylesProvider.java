package io.gitlab.zartc.customlog.customstyles;

public interface CustomStylesProvider {
    CustomStyles create();
}

/* EOF */
