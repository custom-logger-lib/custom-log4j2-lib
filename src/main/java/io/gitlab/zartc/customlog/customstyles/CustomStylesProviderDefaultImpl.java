package io.gitlab.zartc.customlog.customstyles;

public class CustomStylesProviderDefaultImpl implements CustomStylesProvider {
    @Override
    public CustomStyles create() {
        return new CustomStylesDefaultImpl();
    }
}

/* EOF */
