package io.gitlab.zartc.log;

import java.lang.reflect.ParameterizedType;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;


/**
 * A JUnit Jupiter extension that allow test methods to capture and test Log4j2 logs.
 * <p>
 * Require log4j2 in the classpath.
 */
public class LogCaptorExtension implements BeforeEachCallback, AfterEachCallback, ParameterResolver {

    private final List<LogEvent> events = new LinkedList<>();

    private final Appender mockAppender = new AbstractAppender(
            "logCaptor", null, null, false, null) {

        public void append(LogEvent event) {
            events.add(event.toImmutable());
        }

        public boolean stop(long timeout, TimeUnit timeUnit) {
            events.clear();
            return super.stop(timeout, timeUnit);
        }
    };


    public void beforeEach(ExtensionContext context) {
        mockAppender.start();
    }

    public void afterEach(ExtensionContext context) {
        mockAppender.stop();
    }

    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        return LogCaptor.class.equals(parameterContext.getParameter().getType());
    }

    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        if (parameterContext.getParameter().getParameterizedType() instanceof ParameterizedType) {
            var store = extensionContext.getStore(ExtensionContext.Namespace.create(getClass()));
            var actualTypeArgument = ((ParameterizedType)parameterContext.getParameter().getParameterizedType()).getActualTypeArguments()[0];
            return store.getOrComputeIfAbsent((Class<?>)actualTypeArgument, LogCaptor::new);
        }
        return null;
    }

    /**
     * The object to inject into test method.
     *
     * @param <T> the class/logger for which to capture logs
     */
    public class LogCaptor<T> implements ExtensionContext.Store.CloseableResource {
        private final Logger logger;

        public LogCaptor(Class<T> clazz) {
            logger = (Logger)LogManager.getLogger(clazz);
            logger.addAppender(mockAppender);
            logger.setAdditive(false);
            logger.setLevel(Level.INFO);
        }


        /**
         * Overrides the log level property of the target logger. This may result that the overridden property
         * of the target logger is still active even though a new instance of {@link LogCaptor} has been created.
         */
        public void setLogLevelTo(Level level) {
            logger.setLevel(level);
        }

        /**
         * same as calling {@linkplain  #setLogLevelTo(Level)} with Level.INFO.
         */
        public void setLogLevelToInfo() {
            setLogLevelTo(Level.INFO);
        }

        /**
         * same as calling {@linkplain  #setLogLevelTo(Level)} with Level.DEBUG.
         */
        public void setLogLevelToDebug() {
            setLogLevelTo(Level.DEBUG);
        }

        /**
         * same as calling {@linkplain  #setLogLevelTo(Level)} with Level.ALL.
         */
        public void setLogLevelToAll() {
            setLogLevelTo(Level.ALL);
        }

        /**
         * same as calling {@linkplain  #setLogLevelTo(Level)} with Level.TRACE.
         */
        public void setLogLevelToTrace() {
            setLogLevelTo(Level.TRACE);
        }


        /**
         * same as calling {@linkplain  #messages(Level)} with Level.FATAL.
         */
        public List<String> fatalMessages() {
            return messages(Level.FATAL);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.ERROR.
         */
        public List<String> errorMessages() {
            return messages(Level.ERROR);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.WARN.
         */
        public List<String> warnMessages() {
            return messages(Level.WARN);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.INFO.
         */
        public List<String> infoMessages() {
            return messages(Level.INFO);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.DEBUG.
         */
        public List<String> debugMessages() {
            return messages(Level.DEBUG);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.TRACE.
         */
        public List<String> traceMessages() {
            return messages(Level.TRACE);
        }

        /**
         * same as calling {@linkplain  #messages(Level)} with Level.ALL.
         */
        public List<String> allMessages() {
            return messages(Level.ALL);
        }

        /**
         * Get the messages captured at the specified log level.
         *
         * @param level the log level like INFO, DEBUG. use ALL to get all messages.
         *
         * @return a list of String
         */
        public List<String> messages(Level level) {
            return events(level).stream().map(e -> e.getMessage().getFormattedMessage()).collect(Collectors.toList());
        }

        /**
         * Get the ContextData associated to captured LogEvent at the specified log level.
         *
         * @param level the log level like INFO, DEBUG. use ALL to get all messages.
         *
         * @return a list of Map of String
         */
        public List<Map<String, String>> contextData(Level level) {
            return events(level).stream().map(e -> e.getContextData().toMap()).collect(Collectors.toList());
        }

        /**
         * Get the LogEvent captured at the specified log level.
         *
         * @param level the log level like INFO, DEBUG. use ALL to get all events.
         *
         * @return a list of LogEvent
         */
        public List<LogEvent> events(Level level) {
            Predicate<LogEvent> filter = (level == Level.ALL) ? e -> true : e -> e.getLevel() == level;
            return events.stream().filter(filter).collect(Collectors.toList());
        }

        /**
         * Clear the captured list of log events.
         * Call this method only if you need to clear the log event list in the plain middle
         * of one of your test. There is no need to call this method at the end of your tests
         * because it is called automatically by the LogCaptorExtension.
         */
        public void clear() {
            events.clear();
        }

        /**
         * You do not need to call this method because it is called automatically by the LogCaptorExtension.
         */
        public void close() {
            // remove the appender we added earlier in the singleton logger
            logger.removeAppender(mockAppender); // important!
        }
    }
}
