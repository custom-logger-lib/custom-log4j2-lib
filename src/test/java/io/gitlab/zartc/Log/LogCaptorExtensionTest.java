package io.gitlab.zartc.Log;

import static org.assertj.core.api.Assertions.*;
import static org.slf4j.LoggerFactory.*;

import org.apache.logging.log4j.Level;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;

import io.gitlab.zartc.log.LogCaptorExtension;


@ExtendWith(LogCaptorExtension.class)
class LogCaptorExtensionTest {
    private static final Logger log = getLogger(LogCaptorExtensionTest.class);

    @Test
    void contain_message_in_specific_order_1(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        log.info("first info message");
        log.info("second info message");

        assertThat(logCaptor.infoMessages()).containsExactly("first info message", "second info message");
    }

    @Test
    void contain_message_in_specific_order_2(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        log.info("second info message");
        log.info("first info message");

        assertThat(logCaptor.infoMessages()).containsExactly("second info message", "first info message");
    }

    @Test
    void contain_message_in_different_level_1(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        logCaptor.setLogLevelToDebug();

        log.info("info message");
        log.debug("debug message");

        assertThat(logCaptor.allMessages()).containsExactly("info message", "debug message");

        assertThat(logCaptor.infoMessages()).containsExactly("info message");
        assertThat(logCaptor.debugMessages()).containsExactly("debug message");
    }

    @Test
    void contain_message_in_different_level_2(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        log.info("info message");
        log.debug("debug message");

        assertThat(logCaptor.allMessages()).containsExactly("info message");

        assertThat(logCaptor.infoMessages()).containsExactly("info message");

        assertThat(logCaptor.debugMessages()).isEmpty();
    }
    @Test
    void contains_context1(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        log.info("info message");
        log.debug("debug message");
        assertThat(logCaptor.contextData(Level.INFO)).isNotEmpty();
        assertThat(logCaptor.contextData(Level.DEBUG)).isEmpty();
    }
    @Test
    void contains_context2(LogCaptorExtension.LogCaptor<LogCaptorExtensionTest> logCaptor) {
        logCaptor.setLogLevelToDebug();
        log.info("info message");
        log.debug("debug message");
        assertThat(logCaptor.contextData(Level.INFO)).isNotEmpty();
        assertThat(logCaptor.contextData(Level.DEBUG)).isNotEmpty();
    }
}
