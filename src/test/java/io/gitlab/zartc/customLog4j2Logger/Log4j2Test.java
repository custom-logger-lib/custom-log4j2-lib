package io.gitlab.zartc.customLog4j2Logger;

import static org.apache.logging.log4j.LogManager.*;
import static org.assertj.core.api.Assertions.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

class Log4j2Test {
    private static final Logger log = getLogger(Log4j2Test.class);

    static {
        Configurator.setRootLevel(Level.ALL);
    }

    @Test
    @Tag("manual")
    void loggerTest() {
        log.info("populating ThreadContext");
        ThreadContext.put("tenantId", "12345");
        ThreadContext.put("traceId", "6789");
        ThreadContext.put("spanId", "6789-a");

        log.fatal("fatalMsg");
        log.error("errorMsg");
        log.warn("warnMsg");
        log.info("infoMsg");
        log.debug("debugMsg");
        log.trace("traceMsg");
        assertThat(1+1-1).isEqualTo(1);
    }

    @Test
    @Tag("manual")
    void exceptionTracing() {
        try {
            try {
                exceptionThrowing();
            } catch (Exception e) {
                throw new RuntimeException("kaboom", e);
            }
        } catch (Exception e) {
            log.trace("boom", e);
        }
        assertThat(1+1-1).isEqualTo(1);
    }

    @Test
    @Tag("manual")
    void exceptionCatching() {
        try {
            exceptionThrowing();
        } catch (Exception e) {
            log.catching(e);
        }
        assertThat(1+1-1).isEqualTo(1);
    }

    void exceptionThrowing() {
        throw log.throwing(new IllegalArgumentException("sneaky"));
    }

    @Test
    @Tag("manual")
    void entryExitFlowTracing() {
        var entryMessage = log.traceEntry("entryExitFlowTracing");

        var computedString1 = computeString("gary");
        var computedString2 = computeString("billy");

        log.traceExit(entryMessage, computedString1 + ", " + computedString2);
        assertThat(1+1-1).isEqualTo(1);
    }

    private String computeString(String name) {
        log.traceEntry("computeString with {}", name);

        var result = "bonjour " + name;

        return log.traceExit("computeString with {}", result);
    }
}

/* EOF */
