package io.gitlab.zartc.customLog4j2Logger;

import static org.assertj.core.api.Assertions.*;
import static org.slf4j.LoggerFactory.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.MDC;

class Slf4jTest {
    private static final Logger log = getLogger(Slf4jTest.class);

    static {
        Configurator.setRootLevel(Level.ALL);
    }

    @Test()
    @Tag("manual")
    void loggerTest() {
        MDC.put("tenantId", "12345");
        MDC.put("traceId", "6789");
        MDC.put("spanId", "6789-a");

//        log.fatal("fatal"); // log.fatal is unavailable with SLF4J facade
        log.error("errorMsg");
        log.warn("warnMsg");
        log.info("infoMsg");
        log.debug("debugMsg");
        log.trace("traceMsg");
        assertThat(1+1-1).isEqualTo(1);
    }

    @Test
    @Tag("manual")
    void exceptionTracing() {
        try {
            try {
                exceptionThrowing();
            } catch (Exception e) {
                throw new RuntimeException("kaboom", e);
            }
        } catch (Exception e) {
            log.trace("boom", e);
        }
        assertThat(1+1-1).isEqualTo(1);
    }

    void exceptionThrowing() {
        throw new IllegalArgumentException("sneaky");
    }
}

/* EOF */
