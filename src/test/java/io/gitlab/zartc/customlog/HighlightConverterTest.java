package io.gitlab.zartc.customlog;

import static org.assertj.core.api.Assertions.*;

import org.apache.logging.log4j.core.config.Configuration;
import org.junit.jupiter.api.Test;

class HighlightConverterTest {

    @Test
    void newInstance1() {
        // GIVEN
        Configuration config = null;
        var options = new String[] { "STYLE=DEFAULT,disableAnsi=true,noConsoleNoAnsi=true" };
        // WHEN
        var highlightConverter = HighlightConverter.newInstance(config, options);
        // THEN
        assertThat(highlightConverter).isNotNull();
    }

    @Test
    void newInstance_return_null_when_options_contains_null() {
        // GIVEN
        Configuration config = null;
        var options = new String[] { null };
        // WHEN
        var highlightConverter = HighlightConverter.newInstance(config, options);
        // THEN
        assertThat(highlightConverter).isNull();
    }

    @Test
    void newInstance_return_null_when_options_is_null() {
        // GIVEN
        Configuration config = null;
        String[] options = null;
        // WHEN
        var highlightConverter = HighlightConverter.newInstance(config, options);
        // THEN
        assertThat(highlightConverter).isNull();
    }
}

/* EOF */
