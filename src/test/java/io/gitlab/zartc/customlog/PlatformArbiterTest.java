package io.gitlab.zartc.customlog;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PlatformArbiterTest {
    @ParameterizedTest
    @CsvSource({
            "LOG_FORMAT,,PLAIN,Linux,false",        // simulate variable not defined
            "LOG_FORMAT,,PLAIN,Windows,true",       // simulate variable not defined
            "LOG_FORMAT,'',PLAIN,Linux,false",      // variable defined but no value assign
            "LOG_FORMAT,'',PLAIN,Windows,false",    // variable defined but no value assign
            "LOG_FORMAT,plain,PLAIN,Linux,true",
            "LOG_FORMAT,plain,PLAIN,Windows,true",
            "LOG_FORMAT,text-plain,PLAIN,Linux,true",
            "LOG_FORMAT,text-plain,PLAIN,Windows,true",
            "LOG_FORMAT,text-json,PLAIN,Linux,false",
            "LOG_FORMAT,text-json,PLAIN,Windows,false" })
    void test_platformArbiter_isCondition(String propertyName, String propertyValue, String matchingValue, String platform, boolean expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(propertyName)).thenReturn(propertyValue);

        var platformArbiter = spy(new PlatformArbiter(propertyName, matchingValue));
        when(platformArbiter.createVarFinder()).thenReturn(varFinder);
        when(platformArbiter.getOsName()).thenReturn(platform);

        // WHEN
        boolean condition = platformArbiter.isCondition();

        // THEN
        verify(varFinder).getEnv(propertyName);
        verify(varFinder).getSys(propertyName);
        verify(varFinder).lookup(propertyName);
        verifyNoMoreInteractions(varFinder);

        assertThat(condition).isEqualTo(expected);
    }

    @Test
    void test_platformArbiter_builder_expect_propertyName() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new PlatformArbiter.Builder().build();
        });
    }

    @Test
    void test_platformArbiter_builder_expect_non_null_propertyName() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new PlatformArbiter.Builder()
                    .setPropertyName(null)
                    .build();
        });
    }

    @Test
    void test_platformArbiter_builder_expect_matchingValue() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new PlatformArbiter.Builder()
                    .setPropertyName("LOG_IN_FILE")
                    .build();
        });
    }

    @Test
    void test_platformArbiter_builder_expect_non_null_matchingValue() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new PlatformArbiter.Builder()
                    .setPropertyName("LOG_IN_FILE")
                    .setMatchingValue(null)
                    .build();
        });
    }

    @Test
    void test_newBuilder() {
        // WHEN
        var builder = PlatformArbiter.newBuilder().asBuilder();

        // THEN
        assertThat(builder).isNotNull();
    }
}
