package io.gitlab.zartc.customlog;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class VarArbiterTest {

    @ParameterizedTest
    @CsvSource({
            "LOG_FORMAT,,PLAIN,false",      // simulate variable not defined
            "LOG_FORMAT,'',PLAIN,false",    // variable defined but no value assign
            "LOG_FORMAT,plain,PLAIN,true",
            "LOG_FORMAT,text-plain,PLAIN,true",
            "LOG_FORMAT,text-json,PLAIN,false" })
    void test_varArbiter_isCondition(String propertyName, String propertyValue, String matchingValue, boolean expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(propertyName)).thenReturn(propertyValue);

        var varArbiter = spy(new VarArbiter(propertyName, matchingValue));
        when(varArbiter.createVarFinder()).thenReturn(varFinder);

        // WHEN
        boolean condition = varArbiter.isCondition();

        // THEN
        verify(varFinder).getEnv(propertyName);
        verify(varFinder).getSys(propertyName);
        verify(varFinder).lookup(propertyName);
        verifyNoMoreInteractions(varFinder);

        assertThat(condition).isEqualTo(expected);
    }

    @Test
    void test_varArbiter_builder_expect_propertyName() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new VarArbiter.Builder().build();
        });
    }

    @Test
    void test_varArbiter_builder_expect_non_null_propertyName() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new VarArbiter.Builder()
                    .setPropertyName(null)
                    .build();
        });
    }

    @Test
    void test_varArbiter_builder_expect_matchingValue() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new VarArbiter.Builder()
                    .setPropertyName("LOG_IN_FILE")
                    .build();
        });
    }

    @Test
    void test_varArbiter_builder_expect_non_null_matchingValue() {
        assertThatIllegalArgumentException().isThrownBy(() -> {
            new VarArbiter.Builder()
                    .setPropertyName("LOG_IN_FILE")
                    .setMatchingValue(null)
                    .build();
        });
    }

    @Test
    void test_newBuilder() {
        // WHEN
        var builder = VarArbiter.newBuilder().asBuilder();

        // THEN
        assertThat(builder).isNotNull();
    }
}
