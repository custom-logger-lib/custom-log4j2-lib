package io.gitlab.zartc.customlog;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class VarFinderTest {
    private static final String VAR_NAME = "test";

    @Test
    void when_envVariableSet_expect_envVariable() {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getEnv(VAR_NAME)).thenReturn("env_value");

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.getVariable()).isEqualTo(VAR_NAME);
        assertThat(result.getSource()).isEqualTo(VarFinder.SOURCE_ENV);
        assertThat(result.getValue()).isEqualTo("env_value");

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder, never()).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    @Test
    void when_envVariableNotSet_expect_sysVariable() {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getEnv(VAR_NAME)).thenReturn(null);
        when(varFinder.getSys(VAR_NAME)).thenReturn("sys_value");

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.getVariable()).isEqualTo(VAR_NAME);
        assertThat(result.getSource()).isEqualTo(VarFinder.SOURCE_SYS);
        assertThat(result.getValue()).isEqualTo("sys_value");

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    @Test
    void when_sysVariableNotSet_expect_notFound() {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getEnv(VAR_NAME)).thenReturn(null);
        when(varFinder.getSys(VAR_NAME)).thenReturn(null);

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.getVariable()).isEqualTo(VAR_NAME);
        assertThat(result.getSource()).isEqualTo(VarFinder.SOURCE_NOT_FOUND);
        assertThat(result.getValue()).isNull();

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    // ----------------------------------------------------------------------------------------

    @ParameterizedTest
    @CsvSource(value = {
            "text_plain ,PLAIN",
            " plain-text,PLAIN",
            " plain ,PLAIN",
            "PLAIN, plain " },
            ignoreLeadingAndTrailingWhitespace = false)
    void when_valueMatchExpected_expect_true(String value, String expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(VAR_NAME)).thenReturn(value);

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.match(expected)).isTrue();

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "text_json ,PLAIN",
            " json-text,PLAIN",
            " json ,PLAIN",
            "PLAIN, json " },
            ignoreLeadingAndTrailingWhitespace = false)
    void when_valueDoesNotMatchExpected_expect_false(String value, String expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(VAR_NAME)).thenReturn(value);

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.match(expected)).isFalse();

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    // ----------------------------------------------------------------------------------------

    @ParameterizedTest
    @CsvSource(value = {
            " plain ,PLAIN",
            "PLAIN, plain " },
            ignoreLeadingAndTrailingWhitespace = false)
    void when_valueMatchExactlyExpected_expect_true(String value, String expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(VAR_NAME)).thenReturn(value);

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.matchExactly(expected)).isTrue();

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }

    @ParameterizedTest
    @CsvSource(value = {
            " json ,PLAIN",
            "PLAIN, json " },
            ignoreLeadingAndTrailingWhitespace = false)
    void when_valueDoesNotMatchExactlyExpected_expect_false(String value, String expected) {
        // GIVEN
        var varFinder = spy(VarFinder.class);
        when(varFinder.getSys(VAR_NAME)).thenReturn(value);

        // WHEN
        var result = varFinder.lookup(VAR_NAME);

        // THEN
        assertThat(result.matchExactly(expected)).isFalse();

        // AND
        verify(varFinder).getEnv(VAR_NAME);
        verify(varFinder).getSys(VAR_NAME);
        verify(varFinder).lookup(VAR_NAME);
        verifyNoMoreInteractions(varFinder);
    }
}
