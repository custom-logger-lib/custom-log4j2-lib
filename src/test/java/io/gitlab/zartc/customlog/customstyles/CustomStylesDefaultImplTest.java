package io.gitlab.zartc.customlog.customstyles;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CustomStylesDefaultImplTest {
    private static final CustomStylesDefaultImpl customStyle = new CustomStylesDefaultImpl();

    @Test
    void default_style_not_null() {
        // WHEN
        var defaultStyle = customStyle.getDefault();

        // THEN
        assertThat(defaultStyle).isNotNull();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "DEFAULT",
            "LOGBACK",
            "LOG4J",
            "ZION"})
    void style_not_null(String styleName) {
        // WHEN
        var style = customStyle.get(styleName);

        // THEN
        assertThat(style).isNotNull();
    }

    @Test
    void list() {
        // WHEN
        var styles = customStyle.list();

        // THEN
        assertThat(styles).containsExactlyInAnyOrder("DEFAULT", "LOGBACK", "LOG4J", "ZION");
    }

}
