package io.gitlab.zartc.customlog.customstyles;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CustomStylesProviderDefaultImplTest {

    @Test
    void create() {
        var customStyles = new CustomStylesProviderDefaultImpl().create();

        // THEN
        assertThat(customStyles).isInstanceOf(CustomStylesDefaultImpl.class);
    }
}
